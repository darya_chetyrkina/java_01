package method_array;

import java.util.Scanner;

public class Main {


    public static void printArray(int[] array){

        for(int i: array){
            System.out.print(" " + i + " ");
        }
        System.out.println();
    }

    public static int[] createArrayHandle(){
        /*create integer array with console input and return array*/

        Scanner scanner = new Scanner(System.in);
        System.out.print("Size: ");
        int size = scanner.nextInt();
        int[] array = new int[size];

        System.out.print("Values: ");
        for (int i = 0; i < array.length; i++){
            array[i] = scanner.nextInt();
        }

        return array;
    }

    public static int SumArray(int[] array){
        /*return total sum of array*/

        int sum = 0;

        for(int i: array){
            sum+=i;
        }
        return sum;
    }

    public static int quantityEvenNumber(int [] array){
        /*count quantity even numbers*/

        int quantity = 0;

        for(int i: array){
            if (i%2 == 0){
                quantity++;
            }
        }
        return quantity;
    }

    public static int quantityOwned(int[] array, int a, int b){
        /*count the quantity of number in [a,b]*/
        int quantity = 0;

        for(int i: array){
            if (i >= a & i <=b ){
                quantity++;
            }
        }

        return quantity;
    }

    public static boolean isPositiveNum(int[] array){
        /*count the quantity of positive number in array*/

        boolean answer = true;

        for (int i: array){
            if (i < 0) {
                answer = false;
                break;
            }
        }

        return answer;

    }

    public static void reversArray(int[] array){

        int temp;

        for(int i = 0; i < array.length/2; i++){

            temp = array[i];
            array[i] = array[array.length-1-i];
            array[array.length-1-i] = temp;
        }
    }

    public static void main(String[] args) {

        int[] array = createArrayHandle();
        printArray(array);
        System.out.println("Sum: " + SumArray(array));
        System.out.println("Even numbers: " + quantityEvenNumber(array));
        int a = -10;
        int b = 10;
        System.out.println("Quantity on [" + a + "," + b +"]: " + quantityOwned(array,a,b));
        System.out.println("All is positive? - " + isPositiveNum(array));
        reversArray(array);
        printArray(array);
    }
}
