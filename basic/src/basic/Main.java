package basic;

import java.util.Scanner;

public class Main {

    public static void helloWorld(){
        System.out.println("Hello world!");
    }
    public static void printDoubleNums(){

        double a, b, c;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите три вещественных числа:");
        a = scanner.nextDouble();
        b = scanner.nextDouble();
        c = scanner.nextDouble();

        double sum = a+b+c;//cумма
        System.out.println("сумма " + sum);
        System.out.println("среднее арифметическое " + sum/3);//сред. арифм

        if (a <= b && b <= c){
            System.out.println(a +" " + b + " " + c);

        } else if (a <= c && c <= b){
            System.out.println(a + " " + c + " " + b);

        }else if (b <= a && a <= c){
            System.out.println(b + " " + a + " " + c);
        }else if (b <= c){
            System.out.println(b + " " + c + " " + a);
        }else if (a <= b){
            System.out.println(c + " " + a + " " + b);

        }else {
            System.out.println(c + " " + b + " " + a);
        }

    }

    public static void printIntNums(){

        int a, b, c;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите три целых числа:");
        a = scanner.nextInt();
        b = scanner.nextInt();
        c = scanner.nextInt();

        double sum = a+b+c;//cумма
        System.out.println("сума: " + sum);
        System.out.println("среднее арифметическое: " + sum/3);//сред. арифм

        if (a <= b && b <= c){
            System.out.println(a +" "+ b + " "+ c);

        } else if (a <= c && c <= b){
            System.out.println(a + " " + c +" "+ b);

        }else if (b <= a && a <= c){
            System.out.println(b + " " + a + " "+ c);
        }else if (b <= c){
            System.out.println(b + " " + c +" "+ a);
        }else if (a <= b){
            System.out.println(c +" "+ a +" "+ b);
        }else {
            System.out.println(c +" "+ b +" "+ a);
        }


    }

    public static void quadraticEquation(){

        double a, b, c;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите три вещественных числа:");
        a = scanner.nextDouble();
        b = scanner.nextDouble();
        c = scanner.nextDouble();

        if (a == 0) {
            System.out.println("Уравнение не квадратное");
            return;
        }
        double D = (b*b-4*a*c); //дискриминант
        double x1, x2; //корни

        if (D < 0) {
            System.out.println("У уравнения нет вещественных корней.");

        }else if (D == 0){
            System.out.println("У уравнения есть один вещественный корень.");
            x1 = (b*b-Math.sqrt(D))/(2*a);
            System.out.println(x1);
        }else {
            System.out.println("У уравнения есть два вещественных корня.");
            x1 = (b*b-Math.sqrt(D))/(2*a);
            x2 = (b*b+Math.sqrt(D))/(2*a);
            System.out.println(x1);
            System.out.println(x2);
        }
    }

    public static void tabulationSin(){

        Scanner scanner = new Scanner(System.in);
        System.out.print("Границы:");
        double a = scanner.nextDouble();
        double b = scanner.nextDouble();
        System.out.print("Шаг:");
        double step = scanner.nextDouble();

        double eps = 0.00001;

        for (double i = a; i < b+eps; i+=step){

            System.out.println("sin(" + i + ") " + Math.sin(i));
        }

    }

    public static void solve2LinearEquation(){

        Scanner scanner = new Scanner(System.in);
        System.out.print("Кэффициенты первого уравнения a,b,c:");
        double a = scanner.nextDouble();
        double b = scanner.nextDouble();
        double c = scanner.nextDouble();

        System.out.print("Кэффициенты второго уравнения d,e,f:");
        double d = scanner.nextDouble();
        double e = scanner.nextDouble();
        double f = scanner.nextDouble();

        if ((a*e-b*d) != 0){

            double x1 = (a*f-c*d)/(a*e-b*d);
            double x2 = (c*e-b*f)/(a*e-b*d);
            System.out.println("x1 =" + x1 + " x2 = " + x2);

        } else if ((a*f-c*d) != 0 || (c*e-b*f) != 0){
            System.out.println("нет решений");
        } else {
            System.out.println("бесконечно");
        }

    }

    public static void seriesTaylorExp(){

        Scanner scanner = new Scanner(System.in);
        double precision = 0.0000001;
        System.out.print("x = ");
        double x = scanner.nextDouble();
        double exp = 1;
        double lastTerm = 1;

        for (int i = 1; Math.abs(lastTerm) > precision; i++){

            lastTerm = 1;

            for (int j = 1; j <= i; j++ ){

                lastTerm = lastTerm*x/j;
            }

            exp = exp + lastTerm;
        }

        System.out.println("exp(x) = " + exp);
    }

    public static void main(String[] args) {

        //helloWorld();
        //printDoubleNums();
        //printIntNums();
        //quadraticEquation();
        tabulationSin();
        //solve2LinearEquation();
        //seriesTaylorExp();

    }
}
