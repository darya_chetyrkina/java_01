package Main;

import Point3D.Point3D;
import vector3D.*;

public class Main {

    public static void main(String[] args) {


        ////Точки

        Point3D point1 = new Point3D(-1,2,0);
        Point3D point2 = new Point3D(-1,2,1);
        boolean ret = false;
        if (point1 == point2) {
            ret = true;
        }
        if(point1.equals(point2)) {

        }
        System.out.println("points equals? " + ret);
        ret = false;
        if (point1 == point1) {
            ret = true;
        }
        System.out.println("points equals? " + ret);
        System.out.println(point1);

        ////Вектора

        Vector3D v1 = new Vector3D(1, 1, 1);
        Vector3D v2 = new Vector3D(2, 2, 2);
        Vector3D v3 = new Vector3D(-1, 0, 3);
        System.out.println("Vectors equals?");
        if (v2.equal(v1)) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }
        System.out.print("Sum: ");
        Vector3DProcessor.sum(v1,v2).Print();
        System.out.print("Difference: ");
        Vector3DProcessor.difference(v1, v2).Print();
        System.out.println("Scalar product: " + Vector3DProcessor.scalarProduct(v1, v2));
        System.out.print("Vector product: ");
        Vector3DProcessor.vectorProduct(v1, v2).Print();
        System.out.println("Vectors are collinearity?: " + Vector3DProcessor.isCollinearity(v1, v2));

        ////массивы векторов
        Vector3DArray array_vector = new Vector3DArray(3);
        array_vector.Print();
        array_vector.change(0, v1);
        array_vector.change(1, v2);
        array_vector.change(2, v3);
        array_vector.Print();
        System.out.println("Maximum length: "+ array_vector.maximumLength());
        System.out.println("index v2: "+ array_vector.findVector(v2));
        System.out.println("Total sum: ");
        array_vector.totalSum().Print();
        double[] coeff = new double[3];
        coeff[0] = -1;
        coeff[1] = 1;
        coeff[2] = 0;
        System.out.print("Linear combination: ");
        array_vector.linearCombination(coeff).Print();
        Point3D point = new Point3D(0, 1, 1);
        Point3D[] array_points = array_vector.createArrayOfPoint3DwithVectorShift(point);
        System.out.print("Points array: ");
        for(Point3D p: array_points){
            p.printPoint();
        }

    }



}
