package Point3D;

import java.util.Objects;

public class Point3D {

    private double x, y, z;

    public Point3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public Point3D(){}
    public double getX(){return x;}
    public double getY(){return y;}
    public double getZ(){return z;}
    public void setX(double x){this.x=x;}
    public void setY(double y){this.y=y;}
    public void setZ(double z){this.z=z;}
    public void printPoint(){ System.out.println("(" + x + "," + y + "," + z + ")");}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point3D point3D = (Point3D) o;
        return Double.compare(point3D.x, x) == 0 && Double.compare(point3D.y, y) == 0 && Double.compare(point3D.z, z) == 0;
    }

    @Override
    public String toString() {
        return "Point3D{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, z);
    }
}