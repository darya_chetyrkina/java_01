package vector3D;

import Point3D.Point3D;

public class Vector3DArray {
    private Vector3D[] array;
    private final int size;

    public Vector3DArray(int size){
        this.size = size;
        array = new Vector3D[size];
        for (int i = 0; i < size; i++){
            array[i] = new Vector3D();
        }
    }
    public int length(){return size;}
    public void Print(){
        for(int i = 0; i < length(); i++){
            array[i].Print();
        }
    }
    public void change(int i, Vector3D v){
        /*замена элемента на заданный вектор*/
        if (i < size){
            array[i] = v;
        }
    }
    public double maximumLength(){
        double max_length = 0;
        double length;
        for (int i = 0; i < size; i++){
            length = array[i].lengthVector();
            if (length > max_length){
                max_length = length;
            }
        }
        return max_length;
    }
    public double findVector(Vector3D v){
        int index = -1;
        boolean find = false;
        for (int i = 0; i < size && !find; i++){
            if (array[i].equal(v)) {
                find = true;
                index = i;
            }
        }
        return index;
    }

    public Vector3D totalSum(){
        Vector3D totalSum = new Vector3D();
        for (int i = 0; i < size; i++){
            totalSum = Vector3DProcessor.sum(array[i], totalSum);
        }
        return totalSum;
    }

    public Vector3D linearCombination(double[] array_coeff){
        double x = 0,y = 0,z = 0;
        if (size == array_coeff.length){
            for (int i = 0; i < size; i++){
                x = x + array_coeff[i]*array[i].getX();
                y = y + array_coeff[i]*array[i].getY();
                z = z + array_coeff[i]*array[i].getZ();
            }
        }
        return new Vector3D(x,y,z);

    }
    public Point3D[] createArrayOfPoint3DwithVectorShift(Point3D point){
        Point3D[] array_point = new Point3D[size];
        for (int i = 0; i < size; i++){
            array_point[i] = new Point3D(point.getX()+array[i].getX(),point.getY()+array[i].getY(),point.getZ()+array[i].getZ());
        }
        return array_point;
    }



}
