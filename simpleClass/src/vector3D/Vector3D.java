package vector3D;

import Point3D.Point3D;

public class Vector3D {
    private double x, y, z;//coordinates

    public Vector3D(){}
    public Vector3D(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public Vector3D(Point3D a, Point3D b){
        x = b.getX()- a.getX();
        y = b.getY()- a.getY();
        z = b.getZ()- a.getZ();
    }
    public double lengthVector(){
        return Math.sqrt(x*x+y*y+z*z);
    }
    public double getX(){return x;}
    public double getY(){return y;}
    public double getZ(){return z;}

    public boolean equal(Vector3D vector2){
        boolean ret = false;
        if (x == vector2.getX() && y == vector2.getY() && z == vector2.getZ()){
            ret = true;
        }
        return ret;
    }
    public void Print(){
        System.out.println(x + " " + y + " " + z);
    }

}
