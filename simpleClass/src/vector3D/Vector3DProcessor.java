package vector3D;

public class Vector3DProcessor {

    public static  Vector3D sum(Vector3D vector1, Vector3D vector2){
        /*calculate the sum of 2 vectors*/
        double x,y,z;
        x = vector1.getX() + vector2.getX();
        y = vector1.getY() + vector2.getY();
        z = vector1.getZ() + vector2.getZ();
        return new Vector3D(x,y,z);
    }
    public static Vector3D difference(Vector3D vector1, Vector3D vector2){
        /*calculate the difference of 2 vectors*/
        double x,y,z;
        x = vector1.getX() - vector2.getX();
        y = vector1.getY() - vector2.getY();
        z = vector1.getZ() - vector2.getZ();
        return new Vector3D(x,y,z);
    }
    public static double scalarProduct(Vector3D vector1, Vector3D vector2){
        return vector1.getX()* vector2.getX() + vector1.getY()*vector2.getY() + vector1.getZ()*vector2.getZ();
    }
    public static Vector3D vectorProduct(Vector3D vector1, Vector3D vector2){
        double x = vector1.getY()*vector2.getZ()-vector1.getZ()*vector2.getY();
        double y = -(vector1.getX()*vector2.getZ()-vector1.getZ()*vector2.getX());
        double z = vector1.getX()*vector2.getY()-vector1.getY()*vector2.getX();
        return new Vector3D(x,y,z);
    }
    public static boolean isCollinearity(Vector3D vector1, Vector3D vector2){
        //if vector product 2 vectors equal 0-vector, vectors are collinearity
        boolean res = false;
        Vector3D zeroVector = new Vector3D();
        if (zeroVector.equal(vectorProduct(vector1, vector2))){
            res = true;
        }
        return res;
    }
}
